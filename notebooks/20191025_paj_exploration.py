# ---
# jupyter:
#   jupytext:
#     text_representation:
#       extension: .py
#       format_name: light
#       format_version: '1.3'
#       jupytext_version: 0.8.6
#   kernelspec:
#     display_name: Python 3
#     language: python
#     name: python3
# ---

import os

os.chdir("..")

import pandas as pd

pd.options.display.max_columns = 200

df = (pd.read_csv("data/raw/A201812.csv.gz", nrows=10**7, compression="gzip", sep=";")
      .drop(columns=['Unnamed: 55'])
     )


df.head()

df.shape

df.to_parquet("data/interim/A201812.parquet")

ls -lh data/raw

ls -lh data/interim


