# open_damir

Exploitation de la base Open-Damir

Les données proviennent de la Cnam, via le portail [data gouv](https://www.data.gouv.fr/fr/datasets/open-damir-base-complete-sur-les-depenses-dassurance-maladie-inter-regimes/).

Pour installer le projet, lire le fichier [INSTALL.md](INSTALL.md).

## Installation

La commande `make install` installe le projet. 

Pour une procédure manuelle détaillée, voir le fichier [INSTALL.md](INSTALL.md).

## Usage

Organisation des dossiers :
- [notebooks](notebooks) contient les travaux exploratoires 
- [src](src) contient le code de préparation commun
- [test](tests) contient des tests automatisés

### Bientôt
 
Le script `./main.py` à la racinne permettra d'effectuer les travaux de préparation communs.

`./main.py --help` affiche l'aide 

## Références

- http://open-data-assurance-maladie.ameli.fr/depenses/index.php
- https://github.com/SGMAP-AGD/DAMIR
- https://www.institutdesactuaires.com/global/gene/link.php?doc_id=11745&fg=1
